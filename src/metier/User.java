package metier;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class User {


	private IntegerProperty id;
	private StringProperty login;
	private StringProperty email;
	private StringProperty password;
	private StringProperty role;
	
	
	
	public User() {
		this.id = new SimpleIntegerProperty();
		this.login = new SimpleStringProperty();
		this.email = new SimpleStringProperty();
		this.password = new SimpleStringProperty();

		
	}

	//getter setter de Property
	public IntegerProperty getIdP() {
		return id;
	}

	public void setIdP(IntegerProperty id) {
		this.id = id;
	}

	public StringProperty getLoginP() {
		return login;
	}

	public void setLoginP(StringProperty login) {
		this.login = login;
	}

	public StringProperty getEmailP() {
		return email;
	}

	public void setEmailP(StringProperty email) {
		this.email = email;
	}

	public StringProperty getPasswordP() {
		return password;
	}

	public void setPasswordP(StringProperty password) {
		this.password = password;
	}

	public StringProperty getRoleP() {
		return role;
	}

	public void setRoleP(StringProperty role) {
		this.role = role;
	}
	//getter setter de base

	public int getId() {
		return id.get();
	}
	public void setId(int id) {
		this.id.set(id);;
	}
	public String getLogin() {
		return login.get();
	}
	public void setLogin(String login) {
		this.login.set(login);;
	}
	public String getEmail() {
		return email.get();
	}
	public void setEmail(String email) {
		this.email.set(email);
	}
	public String getPassword() {
		return password.get();
	}
	public void setPassword(String password) {
		this.password.set(password);;
	}
	public String getRole() {
		return role.get();
	}
	public void setRole(String role) {
		this.role.set(role);;
	}




	@Override
	public String toString() {
		return "User [id=" + id + ", login=" + login + ", email="
				+ email + ", password=" + password + ", role=" + role + "]";
	}
}
