package metier;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Livre {

	private StringProperty libelle;
	private StringProperty autor;
	private IntegerProperty year;
	private IntegerProperty id;
	
	
	
	public Livre() {
		this.id = new SimpleIntegerProperty();
		this.autor = new SimpleStringProperty();
		this.year = new SimpleIntegerProperty();
		this.libelle = new SimpleStringProperty();

		
	}

	//getter setter de Property
	public StringProperty getLibelleP() {
		return libelle;
	}

	public void setLibelleP(StringProperty libelle) {
		this.libelle = libelle;
	}

	public StringProperty getAutorP() {
		return autor;
	}

	public void setAutorP(StringProperty autor) {
		this.autor = autor;
	}

	public IntegerProperty getYearP() {
		return year;
	}

	public void setYearP(IntegerProperty year) {
		this.year = year;
	}

	public IntegerProperty getIdP() {
		return id;
	}

	public void setIdP(IntegerProperty id) {
		this.id = id;
	}
	
	//getter setter de base

	public String getLibelle() {
		return libelle.get();
	}
	public void setLibelle(String libelle) {
		this.libelle.set(libelle);;
	}
	public String getAutor() {
		return autor.get();
	}
	public void setAutor(String autor) {
		this.autor.set(autor);;
	}
	public int getYear() {
		return year.get();
	}
	public void setYear(int year) {
		this.year.add(year);
	}
	public int getId() {
		return id.get();
	}
	public void setId(int id) {
		this.id.set(id);;
	}



	@Override
	public String toString() {
		return "Livre [id=" + id + ", libelle=" + libelle + ", autor="
				+ autor + ", year=" + year + "]";
	
	}
}
