package metier;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Dvd {

	private IntegerProperty id;
	private StringProperty productor;
	private IntegerProperty year;
	private StringProperty libelle;
	
	
	
	public Dvd() {
		this.id = new SimpleIntegerProperty();
		this.productor = new SimpleStringProperty();
		this.year = new SimpleIntegerProperty();
		this.libelle = new SimpleStringProperty();

		
	}
	
	//getter setter de Property
	public IntegerProperty getIdP() {
		return id;
	}

	public void setIdP(IntegerProperty id) {
		this.id = id;
	}

	public StringProperty getProductorP() {
		return productor;
	}

	public void setProductorP(StringProperty productor) {
		this.productor = productor;
	}

	public IntegerProperty getYearP() {
		return year;
	}

	public void setYearP(IntegerProperty year) {
		this.year = year;
	}

	public StringProperty getLibelleP() {
		return libelle;
	}

	public void setLibelleP(StringProperty libelle) {
		this.libelle = libelle;
	}

	//getter setter de base
	public int getId() {
		return id.get();
	}
	public void setId(int id) {
		this.id.set(id);;
	}
	public String getProductor() {
		return productor.get();
	}
	public void setProductor(String productor) {
		this.productor.set(productor);;
	}
	public int getYear() {
		return year.get();
	}
	public void setYear(int year) {
		this.year.set(year);;
	}
	public String getLibelle() {
		return libelle.get();
	}
	public void setLibelle(String libelle) {
		this.libelle.set(libelle);;
	}
	
	
	@Override
	public String toString() {
		return "Dvd [id=" + id + ", libelle=" + libelle + ", productor="
				+ productor + ", year=" + year + "]";
}
}
