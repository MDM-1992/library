package controller;

import java.io.IOException;
import java.sql.SQLException;

import dao.DvdDao;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import metier.Dvd;
import metier.User;

public class ListeDvdController {
    @FXML
    private TableView<Dvd> dvdTable;
    @FXML
    private TableColumn<Dvd, String> dvdCol;
   
    @FXML
    private Label NomLabel;
    
    @FXML
    private Label producteurLabel;
    
    @FXML
    private Label anneeLabel;
    
    @FXML
    private Button addButton;

    @FXML
    private Button deleteButton;

    @FXML
    private Button editButton;
    
    @FXML
    private MainAppConnect mainAppConnect;
    
    private User user;
    
    @FXML
    private void initialize() {
        // Initialize la table etablissement avec les colonnes.
    	System.out.println(getDvdData());
    	
    	updateTable();
    	
    	dvdCol.setCellValueFactory(cellData -> cellData.getValue().getLibelleP());
        
        
        // renitialise les details de personne
    	showDvdDetails(null);
        
        
        //cherche les changements de s�lection et affiche les d�tails de la personne quand ils sont modifi�s.
     dvdTable.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> showDvdDetails(newValue));
    }

    
   
    

    private void updateTable() {
		// TODO Auto-generated method stub
    	dvdTable.setItems(getDvdData());
	}


	/**
     * Les donn�es sont une observable list d'Etablissement.
     */
    private ObservableList<Dvd> DvdData = FXCollections.observableArrayList();

    /**
     * Renvoie les donn�es sous forme d' observable liste Etablissement. 
     * @return
     */
    public ObservableList<Dvd> getDvdData() {
    	DvdDao dvddao = new DvdDao();
    	try {
    		DvdData = dvddao.findAll() ;
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return DvdData;
    }
    
    /**
     * Est appel� par le main application pour renvoyer une r�f�rence.
     *
     * @param mainApp
     */
    public void setMainAppConnect(MainAppConnect mainAppAdmin) {
        this.mainAppConnect = mainAppAdmin;

        // Add observable list data to the table
      
    }
    /**
     * Remplit tous les champs de texte pour afficher des d�tails sur l'�tablissement.
     * Si l'�tablissement sp�cifi�e est nulle, tous les champs de texte sont effac�s.
     *
     * @param etablissement the etablissement or null
     */
    private void showDvdDetails(Dvd dvd) {
        if (dvd != null) {
            // Fill the labels with info from the person object.
            NomLabel.setText(dvd.getLibelle());
            anneeLabel.setText(Integer.toString(dvd.getYear()));
            producteurLabel.setText(dvd.getProductor());
            

        } else {
            // Person is null, remove all the text.
            NomLabel.setText("");
            anneeLabel.setText("");
            producteurLabel.setText("");

        }
    }
    /**
     * Appell� quand l'admin clique sur le boutton delete.
     */
    @FXML
    private void handleDeleteDvd() {
    	Dvd dvd =  dvdTable.getSelectionModel().getSelectedItem();
    	DvdDao dvddao = new DvdDao();
    	int nbligne =0;
    	if (dvd != null) {
    		  
    	try {
			 nbligne = dvddao.delete(dvd.getId());
			updateTable() ;
			 
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
    	}else {
            // Alerte si aucun dvd n'est s�lectionn�. 
            Alert alert = new Alert(AlertType.WARNING);
            alert.initOwner(mainAppConnect.getPrimaryStage());
            alert.setTitle("Pas de selection");
            alert.setHeaderText("Pas de livre s�lectionn�");
            alert.setContentText("Veuillez selectionner un DVD.");
            alert.showAndWait();
        }
       if ( nbligne == 1)
       {
    	   
    	   Alert alert = new Alert(AlertType.INFORMATION);
    
           alert.initOwner(mainAppConnect.getPrimaryStage());
           alert.setTitle("Suppression ");
           alert.setHeaderText("Le DVD que vous avez s�l�ctionn� � �t� supprim�.");
           alert.setContentText("Vous vennez de supprimer un DVD.");
           alert.showAndWait();
        } 
    }
    

	/**
     * Appel� quand l'administrateur clique sur le boutton nouveau.
     * Ouverture de dialogue pour appliquer un nouvelle etablissement.
     */
    @FXML
    private void handleNewDvd() {
        Dvd tempDvd = null;
        boolean okClicked = showDvdEditPopup(tempDvd);
        if (okClicked) {
        	
        	updateTable() ;
        }
    }

    /**
     * Appel� quand l'administrateur clique sur le boutton modifier. 
     * Ouverture de dialogue pour modifier les details d'un �tablissement.
     */
    @FXML
    private void handleEditDvd() {
        Dvd selectedDvd = dvdTable.getSelectionModel().getSelectedItem();
        System.out.println(selectedDvd);
        if (selectedDvd != null) {
            boolean okClicked = showDvdEditPopup(selectedDvd);
            if (okClicked) {
            
                showDvdDetails(selectedDvd);
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.initOwner(mainAppConnect.getPrimaryStage());
                alert.setTitle("Modification");
                alert.setHeaderText("Modification d'un Livre");
                alert.setContentText("Vous venez d'effectuer une modification");
                
                alert.showAndWait();
            }

        } else {
            // Nothing selected.
            Alert alert = new Alert(AlertType.WARNING);
            alert.initOwner(mainAppConnect.getPrimaryStage());
            alert.setTitle("Aucune s�lection");
            alert.setHeaderText("Pas de DVD s�lectionn�");
            alert.setContentText("Veuillez s�lectionner un DVD.");

            alert.showAndWait();
        }
    }
    public boolean showDvdEditPopup(Dvd dvd) {
        try {
            // Load the fxml file and create a new stage for the popup dialog.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainAppConnect.class.getClassLoader().getResource("vue/DvdEditPopup.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            // Create the dialog Stage.
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Edit Livre");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(this.mainAppConnect.getPrimaryStage());
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Set the person into the controller.
            DvdEditPopupController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setDvd(dvd);

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();
            return controller.isOkClicked();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
	public void setUser(User user) {
		// TODO Auto-generated method stub
		this.user =user ;
	}
    }

