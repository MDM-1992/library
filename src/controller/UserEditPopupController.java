package controller;

import java.sql.SQLException;


import dao.UserDao;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.PasswordField;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import metier.User;

public class UserEditPopupController {



    @FXML
    private TextField loginField;
    @FXML
    private TextField adresseMailField;
    @FXML
    private PasswordField PasswordField;
    @FXML
    private ComboBox<String> col_role;
    @FXML
    AnchorPane  edit ;
   

    private Stage dialogStage;
    private User user;
    private boolean okClicked = false;

	private String roleVal;
        @FXML
       GridPane grid ;
 
    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
    	ObservableList<String> roleData = FXCollections.observableArrayList() ;
    	roleData.add("[\"ROLE_ADMIN\"]");
    	roleData.add("[\"ROLE_CLIENT\"]");

    	
    	
    	col_role.setItems(roleData);
    	
			
			}
 
    



  
  

    /**
     * Sets the stage of this dialog.
     *
     * @param dialogStage
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    /**
     * Sets the person to be edited in the dialog.
     *
     * @param person
     */
    public void setUser(User user) {
        this.user = user;

        loginField.setText(user.getLogin());
        loginField.setText(user.getLogin());
        adresseMailField.setText(user.getEmail());
        PasswordField.setText(user.getPassword());
       // etablissementField.setText(user.getEtablissement().getLibelle());
        //col_classe.setValue(user.getClasse());
        col_role.setValue(user.getRole());
        

    }
    
    

    /**
     * Returns true if the user clicked OK, false otherwise.
     *
     * @return
     */
    public boolean isOkClicked() {
        return okClicked;
    }

    /**
     * Called when the user clicks ok.
     */
    

    @FXML
    private  void handleOk() {
    	
    	if (isInputValid()) {
    		System.out.println(user+"okk");
        	if (user.getId()== 0) {
        		User user = new User();
    			UserDao userdao = new UserDao();
    			 

	            user.setLogin(loginField.getText());
	            user.setRole(roleVal);
	            user.setEmail(adresseMailField.getText());
	            user.setPassword(PasswordField.getText());

	            System.out.println(user.getLogin()+"ok");
	        
	            }else {

	 	            user.setLogin(loginField.getText());
	 	            user.setRole(roleVal);
	 	            user.setEmail(adresseMailField.getText());
	 	            user.setPassword(PasswordField.getText());
	 	           UserDao userdao = new UserDao();
	            try {
					userdao.Update(user);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
            // update
            
        	}
            okClicked = true;
            dialogStage.close();
        
    }


    /**
     * Called when the user clicks cancel.
     */
    @FXML
    private void handleCancel() {
        dialogStage.close();
    }

    /**
     * Validates the user input in the text fields.
     *
     * @return true if the input is valid
     */
    private boolean isInputValid() {
        String errorMessage = "";

        if (PasswordField.getText() == null || PasswordField.getText().length() == 0) {
            errorMessage += "Mot de passe non valide ! \n";
        }

        if (adresseMailField.getText() == null || adresseMailField.getText().length() == 0) {
            errorMessage += "Email non valide ! ! \n";
        }
        if (loginField.getText() == null || loginField.getText().length() == 0) {
            errorMessage += "Login non valide\n";
        }

        if (col_role.getValue() == null || col_role.getValue().length() == 0) {
            errorMessage += "Role non valide !\n";
        } 
       
        else {
            
        }

        if (errorMessage.length() == 0) {
            return true;
        } else {
            // Show the error message.
            Alert alert = new Alert(AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setTitle("Champs invalide");
            alert.setHeaderText("Veuillez corriger les champs");
            alert.setContentText(errorMessage);

            alert.showAndWait();

            return false;
        }
    }


}

