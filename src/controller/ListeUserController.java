package controller;

import java.io.IOException;
import java.sql.SQLException;

import dao.UserDao;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import metier.User;

public class ListeUserController {
	@FXML
    private TableView<User> userTable;
    
    @FXML
    private TableColumn<User, String> roleTable;
    
    @FXML
    private TableColumn<User, String> loginTable;


    @FXML
    private Label roleLabel;

    @FXML
    private Label adressMailLabel;

    @FXML
    private Label loginLabel;

    @FXML
    private Button ajouter;

    @FXML
    private Button supprimer;

    @FXML
    private Button modifier;

    // Reference to the main application.
    private MainAppConnect mainAppConnect;
    
    private User user;

	public ListeUserController() {
	}
	
    /**
     * Initialise la class controlleur. Cette methode est automatiquement appell�e apr�s
     * que le fichier FXML a �t� charg�.
     */
    @FXML
    private void initialize() {
        // Initialise la table User
    	updateTable() ;
    	roleTable.setCellValueFactory(cellData -> cellData.getValue().getRoleP());
    	loginTable.setCellValueFactory(cellData -> cellData.getValue().getLoginP());
 
         
         
         // renitialise les details de personne
         showUserDetails(null);
         
         
         //cherche les changements de s�lection et affiche les d�tails de la personne quand ils sont modifi�s.
      userTable.getSelectionModel().selectedItemProperty().addListener(
                 (observable, oldValue, newValue) -> showUserDetails(newValue));
    }
    /**
     * Fills all text fields to show details about the person.
     * If the specified person is null, all text fields are cleared.
     *
     * @param person the person or null
     */
    private void showUserDetails(User user) {
        if (user != null) {
            // Remplir les label avec les informations de l'objet personne
            roleLabel.setText(user.getRole());
            adressMailLabel.setText(user.getEmail());
          //  roleLabel.setText(Integer.toString(user.getRole()));
            loginLabel.setText(user.getLogin());
            
        } else {
            // Si User = null alors champs vide
        	roleLabel.setText("");
        	adressMailLabel.setText("");
           loginLabel.setText("");

        }
        
    }
    /**
     * Cette methode est appell�e quand l'utilisateur clique sur le bouton supprimer
     */
    @FXML
    private void handleDeleteUser() {
    	
    	
    	User user =   userTable.getSelectionModel().getSelectedItem();
    	UserDao userd = new UserDao();
    	int nbligne =0;
    	if (user != null) {
    	try {
			 nbligne = userd.delete(user.getId());
			updateTable() ;


		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

    	}else {
            // Alerte si aucun utilisateur s�lectionn�. 
            Alert alert = new Alert(AlertType.WARNING);
            alert.initOwner( this.mainAppConnect.getPrimaryStage());
            alert.setTitle("Pas de selection");
            alert.setHeaderText("Pas d'utilisateur sélectionné");
            alert.setContentText("Veuillez selectionner un utilisateur.");
            alert.showAndWait();
        }
       if ( nbligne == 1)
       {
    	   Alert alert = new Alert(AlertType.WARNING);
           alert.initOwner( this.mainAppConnect.getPrimaryStage());
           alert.setTitle("");
         
           alert.setContentText("Utilisateur bien supprimer");
           alert.showAndWait();
        } 


    	/*roleTable.setCellValueFactory(cellData -> cellData.getValue().getRoleP().asObject());
         nomTable.setCellValueFactory(cellData -> cellData.getValue().getNomP());
         prenomTable.setCellValueFactory(cellData -> cellData.getValue().getPrenomP());*/

    }
    /**
     * The data as an observable list of User.
     */
    private ObservableList<User> userData = FXCollections.observableArrayList();

    /**
     * Returns the data as an observable list of Persons. 
     * @return
     */
    public ObservableList<User> getUserData() {
        return userData;
    }
    
    public void updateTable() {
    	
    	try {
    		UserDao userdao = new UserDao() ;
    		
    		ObservableList<User> liste = userdao.findAll();
    		//System.out.println(liste.get(0));
    		 userTable.setItems(liste);
    		
    		} catch (SQLException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		} catch (ClassNotFoundException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
        	
    }
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>UserEditDialog>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    //Ces m�thodes vont appeler showUserEditDialog(...) de MainApp lorsque l�utilisateur clique sur les boutons Nouveau ou Editer.
    /**
     * Called when the user clicks the new button. Opens a dialog to edit
     * details for a new person.
     */
    
    @FXML
    private void handleNewPerson() {
        User tempUser = new User();
        boolean okClicked =showUserEditDialog(tempUser);
      if (okClicked) {
    	  updateTable() ;
        }
    }

    /**
     * Called when the user clicks the edit button. Opens a dialog to edit
     * details for the selected person.
     */
    @FXML
    private void handleEditUser() {
        User selectedUser = userTable.getSelectionModel().getSelectedItem();
        if (selectedUser != null) {
            boolean okClicked = showUserEditDialog(selectedUser);
            if (okClicked) {
                showUserDetails(selectedUser);
            }

        } else {
            // Nothing selected.
            Alert alert = new Alert(AlertType.WARNING);
            alert.initOwner( this.mainAppConnect.getPrimaryStage());
            alert.setTitle("pas de Selection");
            alert.setHeaderText("Pas d'utilisateur s�lectionn�");
            alert.setContentText("Veuillez selectionner un utilisateur dans le tableau.");

            alert.showAndWait();
        }
    }
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>FinUserEditDialog>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    
    public boolean showUserEditDialog(User user) {
        try {
            // Load the fxml file and create a new stage for the popup dialog.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainAppConnect.class.getResource("/vue/UserEditPopup.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            // Create the dialog Stage.
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Edit User");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(this.mainAppConnect.getPrimaryStage());
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Set the person into the controller.
            UserEditPopupController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setUser(user);

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();

            return controller.isOkClicked();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
    /**
     * Is called by the main application to give a reference back to itself.
     * 
     * @param mainApp
     */
    public void setMainAppConnect(MainAppConnect mainAppAdmin) {
        this.mainAppConnect = mainAppAdmin;
        
       
    }
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		// TODO Auto-generated method stub
		this.user = user ;
	}
    
}


