package controller;

import java.awt.Event;
import java.io.IOException;

import application.MainAppAdmin;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;
import metier.User;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;

/**
 * @author Marc Missler
 * @notes Ce controller sert � afficher la page d'accueil de l'administrateur.
 * @functionality bouton liste utilisateur / bouton liste mati�re 
 * / bouton liste etablissement / bouton deconnexion
 */
public class AdminAccueilController {
	
    @FXML
    private Button btnUser;

    @FXML
    private Button btnLivre;

    @FXML
    private Button btnDvd;

    
	private MainAppConnect mainAppConnect;

	private User user;
	


    public void showListeUser(ActionEvent evt) {
        try {
            // Charge la listeUser.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainAppConnect.class.getResource("/vue/adminUser.fxml"));
            AnchorPane listeUser = (AnchorPane) loader.load();
            
            // Place la listeUser au centre de l'admin menu
            mainAppConnect.getConnect().setCenter(listeUser);
            
         //  Donne au controller l'acc�s � mainAppConnect
           ListeUserController controller = loader.getController();
            System.out.println(controller);
           controller.setMainAppConnect(mainAppConnect);
            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public void showListeLivre(ActionEvent evt) {
        try {
            // Charge la listeUser.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainAppAdmin.class.getResource("/vue/adminLivre.fxml"));
            AnchorPane listeLivre = (AnchorPane) loader.load();
            
            // Place la listeUser au centre de l'admin menu
            mainAppConnect.getConnect().setCenter(listeLivre);
          //  
         //  Donne au controller l'acc�s � mainAppAdmin
            ListeLivreController controller = loader.getController();
            System.out.println(controller);
           controller.setMainAppConnect(mainAppConnect);
            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void showListeDvd(ActionEvent evt) {
        try {
            // Charge la listeUser.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainAppAdmin.class.getResource("/vue/adminDvd.fxml"));
            AnchorPane listeDvd = (AnchorPane) loader.load();
            
            // Place la listeUser au centre de l'admin menu
            mainAppConnect.getConnect().setCenter(listeDvd);
            
         //  Donne au controller l'acc�s � mainAppAdmin
            ListeDvdController controller = loader.getController();
            System.out.println(controller);
           controller.setMainAppConnect(mainAppConnect);
            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

	public void setMainAppConnect(MainAppConnect mainAppAdmin) {
        this.mainAppConnect = mainAppAdmin;
        
    }
	public void setUser(User user) {
		// TODO Auto-generated method stub
		this.user =user ;
	}
}


