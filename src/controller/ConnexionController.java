package controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.xml.stream.util.EventReaderDelegate;

import application.MainAppAdmin;
import dao.UserDao;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import metier.User;

/**
 * @author Marc Missler
 *@note Ce controller sert � renvoyer son utilisateur au moment de la connexion
 * vers les pages ou il a une habilitation
 */
public class ConnexionController {

	@FXML
	private TextField login ;
    @FXML
    private PasswordField password;
	
	@FXML
	private BorderPane connect;
	private MainAppConnect mainAppConnect;
	private User user ;
	
	
//===========================================GESTION DES ROLES ================================================================
	/**
     * Renvoi l'utilisateur dans les pages qui lui son d�di�s
     */
	@FXML
public  void connexion(Event evt) throws IOException, SQLException {

		String login1 = login.getText();
		String mdp = password.getText();
		user = 	UserDao.UserVerif(login1, mdp) ;
		
		if (user.getRole().contentEquals("[\"ROLE_ADMIN\"]") ) {
			System.out.println("ok");
			showAdminAccueil();
				
		}else {
			if (user.getRole().contentEquals("[\"ROLE_CLIENT\"]") ) {
				System.out.println("ok");
				showClientAccueil();
				

			}
			
			
			
		}
		}

    //+++++++++++++++++++++++++++++++++++++++++++Affichage-des-pages-D'accueil++++++++++++++++++++++++++++++++++++++++++
    public void showAdminAccueil() {
        try {
            // Charge la listeUser.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainAppAdmin.class.getResource("/vue/adminMenu.fxml"));
            AnchorPane listeUser = (AnchorPane) loader.load();
            
            // Place la listeUser au centre de l'admin menu
            MainAppConnect.getConnect().setCenter(listeUser);
            
         //  Donne au controller l'acc�s � mainAppAdmin
            AdminAccueilController controller = loader.getController();
            System.out.println(controller);
           controller.setMainAppConnect(mainAppConnect);
            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void showClientAccueil() {
        try {
            // Charge la listeUser.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainAppConnect.class.getResource("/vue/profAccueil.fxml"));
            AnchorPane clientAccueil = (AnchorPane) loader.load();
            
            // Place la listeUser au centre de l'admin menu
            MainAppConnect.getConnect().setCenter(clientAccueil);
            
         //  Donne au controller l'acc�s � mainAppAdmin
            ClientAccueilController controller = loader.getController();
            System.out.println(controller);
           controller.setMainAppConnect(mainAppConnect);
            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

	public User getUser() {
		return user;
	}




	public void setMainAppConnect(MainAppConnect mainAppConnect) {
		// TODO Auto-generated method stub
		this.mainAppConnect = mainAppConnect ;
	}


	public void setUser(User user) {
		// TODO Auto-generated method stub
		this.user = user ;
	}
    }