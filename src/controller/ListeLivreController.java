package controller;

import java.io.IOException;
import java.sql.SQLException;


import dao.LivreDao;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import metier.Livre;
import metier.User;

public class ListeLivreController {
    @FXML
    private TableView<Livre> livreTable;
    @FXML
    private TableColumn<Livre, String> livreCol;
   
    @FXML
    private Label NomLabel;
    
    @FXML
    private Label auteurLabel;
    
    @FXML
    private Label anneeLabel;
    
    @FXML
    private Button addButton;

    @FXML
    private Button deleteButton;

    @FXML
    private Button editButton;
    
    @FXML
    private MainAppConnect mainAppConnect;
    
    private User user;
    
    @FXML
    private void initialize() {
        // Initialize la table etablissement avec les colonnes.
    	System.out.println(getLivreData());
    	
    	updateTable();
    	
    	livreCol.setCellValueFactory(cellData -> cellData.getValue().getLibelleP());
        
        
        // renitialise les details de personne
        showLivreDetails(null);
        
        
        //cherche les changements de s�lection et affiche les d�tails de la personne quand ils sont modifi�s.
     livreTable.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> showLivreDetails(newValue));
    }

    
   
    

    private void updateTable() {
		// TODO Auto-generated method stub
    	livreTable.setItems(getLivreData());
	}


	/**
     * Les donn�es sont une observable list d'Etablissement.
     */
    private ObservableList<Livre> LivreData = FXCollections.observableArrayList();

    /**
     * Renvoie les donn�es sous forme d' observable liste Etablissement. 
     * @return
     */
    public ObservableList<Livre> getLivreData() {
    	LivreDao etabdao = new LivreDao();
    	try {
    		LivreData = etabdao.findAll() ;
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return LivreData;
    }
    
    /**
     * Est appel� par le main application pour renvoyer une r�f�rence.
     *
     * @param mainApp
     */
    public void setMainAppConnect(MainAppConnect mainAppAdmin) {
        this.mainAppConnect = mainAppAdmin;

        // Add observable list data to the table
      
    }
    /**
     * Remplit tous les champs de texte pour afficher des d�tails sur l'�tablissement.
     * Si l'�tablissement sp�cifi�e est nulle, tous les champs de texte sont effac�s.
     *
     * @param etablissement the etablissement or null
     */
    private void showLivreDetails(Livre livre) {
        if (livre != null) {
            // Fill the labels with info from the person object.
            NomLabel.setText(livre.getLibelle());
            anneeLabel.setText(Integer.toString(livre.getYear()));
            auteurLabel.setText(livre.getAutor());
            

        } else {
            // Person is null, remove all the text.
            NomLabel.setText("");
            anneeLabel.setText("");
            auteurLabel.setText("");

        }
    }
    /**
     * Appell� quand l'admin clique sur le boutton delete.
     */
    @FXML
    private void handleDeleteEtablissement() {
    	Livre livre =  livreTable.getSelectionModel().getSelectedItem();
    	LivreDao livreDao = new LivreDao();
    	int nbligne =0;
    	if (livre != null) {
    		  
    	try {
			 nbligne = livreDao.delete(livre.getId());
			updateTable() ;
			 
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
    	}else {
            // Alerte si aucun livre n'est s�lectionn�. 
            Alert alert = new Alert(AlertType.WARNING);
            alert.initOwner(mainAppConnect.getPrimaryStage());
            alert.setTitle("Pas de selection");
            alert.setHeaderText("Pas de livre s�lectionn�");
            alert.setContentText("Veuillez selectionner un livre.");
            alert.showAndWait();
        }
       if ( nbligne == 1)
       {
    	   
    	   Alert alert = new Alert(AlertType.INFORMATION);
    
           alert.initOwner(mainAppConnect.getPrimaryStage());
           alert.setTitle("Suppression ");
           alert.setHeaderText("Le livre que vous avez s�l�ctionn� � �t� supprim�.");
           alert.setContentText("Vous vennez de supprimer un livre.");
           alert.showAndWait();
        } 
    }
    

	/**
     * Appel� quand l'administrateur clique sur le boutton nouveau.
     * Ouverture de dialogue pour appliquer un nouvelle etablissement.
     */
    @FXML
    private void handleNewLivre() {
        Livre tempLivre = null;
        boolean okClicked = showLivreEditPopup(tempLivre);
        if (okClicked) {
        	
        	updateTable() ;
        }
    }

    /**
     * Appel� quand l'administrateur clique sur le boutton modifier. 
     * Ouverture de dialogue pour modifier les details d'un �tablissement.
     */
    @FXML
    private void handleEditLivre() {
        Livre selectedLivre = livreTable.getSelectionModel().getSelectedItem();
        System.out.println(selectedLivre);
        if (selectedLivre != null) {
            boolean okClicked = showLivreEditPopup(selectedLivre);
            if (okClicked) {
            
                showLivreDetails(selectedLivre);
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.initOwner(mainAppConnect.getPrimaryStage());
                alert.setTitle("Modification");
                alert.setHeaderText("Modification d'un Livre");
                alert.setContentText("Vous venez d'effectuer une modification");
                
                alert.showAndWait();
            }

        } else {
            // Nothing selected.
            Alert alert = new Alert(AlertType.WARNING);
            alert.initOwner(mainAppConnect.getPrimaryStage());
            alert.setTitle("Aucune s�lection");
            alert.setHeaderText("Pas de livre s�lectionn�");
            alert.setContentText("Veuillez s�lectionner un livre.");

            alert.showAndWait();
        }
    }
    public boolean showLivreEditPopup(Livre livre) {
        try {
            // Load the fxml file and create a new stage for the popup dialog.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainAppConnect.class.getClassLoader().getResource("vue/LivreEditPopup.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            // Create the dialog Stage.
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Edit Livre");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(this.mainAppConnect.getPrimaryStage());
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Set the person into the controller.
            LivreEditPopupController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setLivre(livre);

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();
            return controller.isOkClicked();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
	public void setUser(User user) {
		// TODO Auto-generated method stub
		this.user =user ;
	}
    }

