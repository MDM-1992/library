package controller;

import metier.Dvd;
import metier.Livre;

import java.sql.SQLException;

import dao.DvdDao;
import dao.LivreDao;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

public class ListeClientDvdController {

    @FXML
    private TableView<Dvd> dvdviewTable;

    @FXML
    private TableColumn<Dvd, String> dvdTable;

    @FXML
    private Label dvdLabel;

    @FXML
    private Label producteurLabel;

    @FXML
    private Label anneeLabel;

    
    // Reference to the main application.

   	private MainAppConnect mainAppConnect;
       




   	public ListeClientDvdController() {
   	}
   	
       /**
        * Initialise la class controlleur. Cette methode est automatiquement appell�e apr�s 
        * que le fichier FXML a �t� charg�.
        */
       @FXML
       private void initialize() {
           // Initialise la table User
       	updateTable() ;
       	dvdTable.setCellValueFactory(cellData -> cellData.getValue().getLibelleP());

            
            
            // renitialise les details de personne
            showDvdDetails(null);
            
            
            //cherche les changements de s�lection et affiche les d�tails de la personne quand ils sont modifi�s.
         dvdviewTable.getSelectionModel().selectedItemProperty().addListener(
                    (observable, oldValue, newValue) -> showDvdDetails(newValue));
       }
       /**
        * Fills all text fields to show details about the person.
        * If the specified person is null, all text fields are cleared.
        *
        * @param person the person or null
        */
       private void showDvdDetails(Dvd dvd) {
           if (dvd != null) {
               // Remplir les label avec les informations de l'objet personne
               dvdLabel.setText(dvd.getLibelle());
               producteurLabel.setText(dvd.getProductor());
               anneeLabel.setText(String.valueOf(dvd.getYear()));

           
           } else {
               // Si User = null alors champs vide
        	   dvdLabel.setText("");
        	   producteurLabel.setText("");
        	   anneeLabel.setText("");

           }
           
       }

       /**
        * The data as an observable list of User.
        */
       private ObservableList<Dvd> dvdData = FXCollections.observableArrayList();



       /**
        * Returns the data as an observable list of Persons. 
        * @return
        */
       public ObservableList<Dvd> getDvdData() {
           return dvdData;
       }
       
       public void updateTable() {
       	
       	try {
       		DvdDao dvddao = new DvdDao() ;
       		//EtablissementDAO etablissementdao = new EtablissementDAO();
       		
       		ObservableList<Dvd> liste = dvddao.findAll();
       		//System.out.println(liste.get(0));
       		 dvdviewTable.setItems(liste);
       		
       		} catch (SQLException e) {
       			// TODO Auto-generated catch block
       			e.printStackTrace();
       		} catch (ClassNotFoundException e) {
       			// TODO Auto-generated catch block
       			e.printStackTrace();
       		}
           	
       }

       
           /**
            * Is called by the main application to give a reference back to itself.
            * 
            * @param mainAppProf
            */


           // Add observable list data to the table
         

       	public void setMainAppConnect(MainAppConnect mainAppAdmin) {
               this.mainAppConnect = mainAppAdmin;
               
           }
       
   }



