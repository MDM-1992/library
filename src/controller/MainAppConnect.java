package controller;

import java.io.IOException;

import application.MainAppAdmin;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import metier.User;


public class MainAppConnect extends Application {
	private Stage primaryStage;
	private User user;
    private static BorderPane connect;
   
    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("library");
        try {
            // Charge l'adminMenu du fichier fxml.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainAppAdmin.class.getResource("/vue/connexion.fxml"));
            connect = (BorderPane) loader.load();
            ConnexionController controller = loader.getController();
            System.out.println(controller);
           controller.setMainAppConnect(this);
            //  Affiche la scene contenant l'adminMenu
            Scene scene = new Scene(connect);
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static BorderPane getConnect() {
		return connect;
	}

	public static void setConnect(BorderPane connect) {
		connect = connect;
	}
	public Stage getPrimaryStage() {
        return primaryStage;
    }

    public static void main(String[] args) {
        launch(args);
    }


	public void setUser(User user) {
		// TODO Auto-generated method stub
		this.user = user ;
	}
}
