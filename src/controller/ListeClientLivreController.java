package controller;

import metier.Livre;


import java.sql.SQLException;

import dao.LivreDao;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

public class ListeClientLivreController {

    @FXML
    private TableView<Livre> bookTable;

    @FXML
    private TableColumn<Livre, String> livreTable;

    @FXML
    private Label libelleLabel;

    @FXML
    private Label auteurLabel;

    @FXML
    private Label anneeLabel;

    
    // Reference to the main application.

   	private MainAppConnect mainAppConnect;
       



   	public ListeClientLivreController() {
   	}
   	
       /**
        * Initialise la class controlleur. Cette methode est automatiquement appell�e apr�s 
        * que le fichier FXML a �t� charg�.
        */
       @FXML
       private void initialize() {
           // Initialise la table User
       	updateTable() ;
       	livreTable.setCellValueFactory(cellData -> cellData.getValue().getLibelleP());

            
            
            // renitialise les details de personne
            showLivreDetails(null);
            
            
            //cherche les changements de s�lection et affiche les d�tails de la personne quand ils sont modifi�s.
         bookTable.getSelectionModel().selectedItemProperty().addListener(
                    (observable, oldValue, newValue) -> showLivreDetails(newValue));
       }
       /**
        * Fills all text fields to show details about the person.
        * If the specified person is null, all text fields are cleared.
        *
        * @param person the person or null
        */
       private void showLivreDetails(Livre livre) {
           if (livre != null) {
               // Remplir les label avec les informations de l'objet personne
               libelleLabel.setText(livre.getLibelle());
               auteurLabel.setText(livre.getAutor());
               anneeLabel.setText(String.valueOf(livre.getYear()));

           
           } else {
               // Si User = null alors champs vide
        	   libelleLabel.setText("");
        	   auteurLabel.setText("");
        	   anneeLabel.setText("");

           }
           
       }

       /**
        * The data as an observable list of User.
        */
       private ObservableList<Livre> livreData = FXCollections.observableArrayList();



       /**
        * Returns the data as an observable list of Persons. 
        * @return
        */
       public ObservableList<Livre> getLivreData() {
           return livreData;
       }
       
       public void updateTable() {
       	
       	try {
       		LivreDao livredao = new LivreDao() ;
       		//EtablissementDAO etablissementdao = new EtablissementDAO();
       		
       		ObservableList<Livre> liste = livredao.findAll();
       		//System.out.println(liste.get(0));
       		 bookTable.setItems(liste);
       		
       		} catch (SQLException e) {
       			// TODO Auto-generated catch block
       			e.printStackTrace();
       		} catch (ClassNotFoundException e) {
       			// TODO Auto-generated catch block
       			e.printStackTrace();
       		}
           	
       }

       
           /**
            * Is called by the main application to give a reference back to itself.
            * 
            * @param mainAppProf
            */


           // Add observable list data to the table
         

       	public void setMainAppConnect(MainAppConnect mainAppAdmin) {
               this.mainAppConnect = mainAppAdmin;
               
           }
       
   }



