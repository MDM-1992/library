package controller;

import java.sql.SQLException;

import dao.LivreDao;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import metier.Livre;
import metier.User;

public class LivreEditPopupController {

    @FXML
    private Label titreLabel;

    @FXML
    private Label auteurLabel;

    @FXML
    private TextField titreField;

    @FXML
    private TextField auteurField;

    @FXML
    private Label anneeLabel;

    @FXML
    private TextField anneeField;

    @FXML
    private Button handleOk;
	   


	    private User user;
	    private Stage dialogStage;
	    private Livre livre ;
	    private boolean okClicked = false;

	    /**
	     * Initializes the controller class. This method is automatically called
	     * after the fxml file has been loaded.
	     */
	    @FXML
	    private void initialize() {
	    }

	    /**
	     * Sets the stage of this dialog.
	     *
	     * @param dialogStage
	     */
	    public void setDialogStage(Stage dialogStage) {
	        this.dialogStage = dialogStage;
	    }

	    /**
	     * Sets the person to be edited in the dialog.
	     *
	     * @param person
	     */
	    public void setLivre(Livre livre) {
	    	
	        this.livre = livre;
	        	if ( livre !=null) {
	        		titreField.setText(livre.getLibelle());
	        		anneeLabel.setText(Integer.toString(livre.getYear()));
	        		auteurField.setText(livre.getAutor());
	}
	    }

	    /**
	     * Returns true if the user clicked OK, false otherwise.
	     *
	     * @return
	     */
	    public boolean isOkClicked() {
	        return okClicked;
	    }

	    /**
	     * Called when the user clicks ok.
	     */
	    @FXML
	    private void handleOk() {
	        if (isInputValid()) {
	        	
	        	if (livre == null) {
	        		livre = new Livre();
	        		livre.setLibelle(titreField.getText());
	        		livre.setYear(Integer.parseInt(anneeField.getText()));
	        		livre.setAutor(auteurField.getText());
	            LivreDao livredao = new LivreDao();
	            try {
	            	livredao.insert(livre);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	            
	        	}else {
					
					livre.setLibelle(titreField.getText());
					livre.setYear(Integer.parseInt(anneeField.getText()));
					livre.setAutor(auteurField.getText());
		            LivreDao livredao = new LivreDao();
		            try {
		            	livredao.Update(livre);
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (ClassNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
	            // update
	            
	        	}
	            okClicked = true;
	            dialogStage.close();
	        
	    }

	    /**
	     * Called when the user clicks cancel.
	     */
	    @FXML
	    private void handleCancel() {
	        dialogStage.close();
	    }

	    /**
	     * Validates the user input in the text fields.
	     *
	     * @return true if the input is valid
	     */
	    private boolean isInputValid() {
	        String errorMessage = "";

	        if (titreField.getText() == null || titreField.getText().length() == 0) {
	            errorMessage += "Livre non valide !";
	        }
	        if (anneeField.getText() == null || anneeField.getText().length() == 0) {
	            errorMessage += "ann�e non valide (N'entrez que des chiffres)!";
	        } else {
	            // try to parse the postal code into an int.
	            try {
	                Integer.parseInt(anneeField.getText());
	            } catch (NumberFormatException e) {
	                errorMessage += "Ann�e non valide !";
	            }
	        
	        if (auteurField.getText() == null || auteurField.getText().length() == 0) {
	            errorMessage += "Auteur non valide !";
	        }


	        }

	        if (errorMessage.length() == 0) {
	            return true;
	        } else {
	            // Show the error message.
	            Alert alert = new Alert(AlertType.ERROR);
	            alert.initOwner(dialogStage);
	            alert.setTitle("Donn�es invalides");
	            alert.setHeaderText("Veuillez entrer les bonnes donn�es.");
	            alert.setContentText(errorMessage);

	            alert.showAndWait();

	            return false;
	        }
	    }
		public void setUser(User user) {
			// TODO Auto-generated method stub
			this.user =user ;
		}
}
