package controller;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import metier.User;

public class ClientAccueilController {
	private MainAppConnect mainAppConnect;
	private User user;
	
    @FXML
    private Button btnLivres;

    @FXML
    private Button btnDvd;

    public void showListeLivre(ActionEvent evt) {
        try {
            // Charge la listeUser.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainAppConnect.class.getResource("/vue/clientLivre.fxml"));
            AnchorPane listeLivre = (AnchorPane) loader.load();
            
            // Place la listeUser au centre de l'admin menu
            mainAppConnect.getConnect().setCenter(listeLivre);
            
         //  Donne au controller l'acc�s � mainAppConnect
           ListeClientLivreController controller = loader.getController();
            System.out.println(controller);
           controller.setMainAppConnect(mainAppConnect);
            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void showListeDvd(ActionEvent evt) {
        try {
            // Charge la listeUser.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainAppConnect.class.getResource("/vue/listeUserProf.fxml"));
            AnchorPane listeUser = (AnchorPane) loader.load();
            
            // Place la listeUser au centre de l'admin menu
            mainAppConnect.getConnect().setCenter(listeUser);
            
         //  Donne au controller l'acc�s � mainAppConnect
           ListeClientDvdController controller = loader.getController();
            System.out.println(controller);
           controller.setMainAppConnect(mainAppConnect);
            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
	public void setMainAppConnect(MainAppConnect mainAppAdmin) {
        this.mainAppConnect = mainAppAdmin;
        
    }
	public void setUser(User user) {
		// TODO Auto-generated method stub
		this.user =user ;
	}
}

