package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import metier.Dvd;
import metier.Livre;

public class LivreDao implements GestionInterface<Livre>{
	
	
	@Override
	public int insert(Livre livre) throws SQLException, ClassNotFoundException {
		// Je me connecte
		Connection co = Connect.getInstance().getConnection();

		// Cr�ation de la requ�te inserer new pers
		String requeteSQL = "INSERT INTO `livre`(`libelle`, `autor`, `year`) "
				+ "VALUES (?,?,?)";

		// pr�parer la requ�te
		PreparedStatement pst = co.prepareStatement(requeteSQL);

		// renvoyer et verifier les donn�es de la requ�te
		pst.setString(1, livre.getLibelle());
		pst.setString(2, livre.getAutor());
		pst.setInt(3, livre.getYear());
		;

		int nbligne = pst.executeUpdate();
		return nbligne;	
	}
	
	public  void Update(Livre livre) throws SQLException, ClassNotFoundException {
		// Je me connecte
		Connection co = Connect.getInstance().getConnection();

		// Cr�ation de la requ�te inserer new pers
		String requeteSQL = "UPDATE livre SET libelle= ?,autor= ?, year = ?WHERE id =?";

		// pr�parer la requ�te
		PreparedStatement pst = co.prepareStatement(requeteSQL);

		// renvoyer et verifier les donn�es de la requ�te
		pst.setString(1, livre.getLibelle());
		pst.setString(2, livre.getAutor());
		pst.setInt(3, livre.getYear());
		
		int nbligne = pst.executeUpdate();

	
		
	}

	@Override
	public int delete(int id) throws SQLException {
		// TODO Auto-generated method stub
		Connection co = Connect.getInstance().getConnection();

		// Cr�ation de la requ�te supprimer etablissement
		String requeteSQL = "DELETE FROM `livre` WHERE id = ?";

		// pr�parer la requ�te
		PreparedStatement pst = co.prepareStatement(requeteSQL);

		// renvoyer et verifier les donn�es de la requ�te
		pst.setInt(1, id);
		
		
		
		int i = pst.executeUpdate();

		return i;
	}
	
	@Override
	public ObservableList<Livre> findAll() throws ClassNotFoundException, SQLException {
      	 ObservableList<Livre> LivreList = FXCollections.observableArrayList();

         // constitution d'une commande bas�e sur une requ�te SQL 
         // en vue d'�tre ex�cut�e sur une connexion donn�e     
         String req = "select * from Livre";

         Connection cnx = Connect.getInstance().getConnection() ;
       int id ;
       String libelle;
       String autor;
       int year;
       
         PreparedStatement pst = cnx.prepareStatement(req);


         ResultSet jeu = pst.executeQuery();
         while(jeu.next()){
        	 id = jeu.getInt("id");
        	 libelle = jeu.getString("libelle");
        	 autor = jeu.getString("autor");
        	 year =jeu.getInt("year");
        	 


        	 
        	 Livre livre = new Livre();
        	 livre.setId(id);
        	 livre.setLibelle(libelle);
        	 livre.setAutor(autor);
        	 livre.setYear(year);
        	 


         }
         jeu.close();
         pst.close();
         cnx.close();
         return LivreList;
	}

	@Override
	public ObservableList<Livre> findBy(int id) throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		return null;
	}





}


