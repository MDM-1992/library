package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import metier.Dvd;
import metier.User;



public class DvdDao implements GestionInterface<Dvd>{

	@Override
	public int insert(Dvd dvd) throws SQLException, ClassNotFoundException {
		// Je me connecte
		Connection co = Connect.getInstance().getConnection();

		// Cr�ation de la requ�te inserer new pers
		String requeteSQL = "INSERT INTO `dvd`(`productor`, `year`, `libelle`) "
				+ "VALUES (?,?,?)";

		// pr�parer la requ�te
		PreparedStatement pst = co.prepareStatement(requeteSQL);

		// renvoyer et verifier les donn�es de la requ�te
		pst.setString(1, dvd.getProductor());
		pst.setInt(2, dvd.getYear());
		pst.setString(3, dvd.getLibelle());
		;

		int nbligne = pst.executeUpdate();
		return nbligne;	
	}
	
	public  void Update(Dvd dvd) throws SQLException, ClassNotFoundException {
		// Je me connecte
		Connection co = Connect.getInstance().getConnection();

		// Cr�ation de la requ�te inserer new pers
		String requeteSQL = "UPDATE dvd SET productor= ?,year= ?, libelle = ?WHERE id =?";

		// pr�parer la requ�te
		PreparedStatement pst = co.prepareStatement(requeteSQL);

		// renvoyer et verifier les donn�es de la requ�te
		pst.setString(1, dvd.getProductor());
		pst.setInt(2, dvd.getYear());
		pst.setString(3, dvd.getLibelle());
		
		int nbligne = pst.executeUpdate();

	
		
	}

	@Override
	public int delete(int id) throws SQLException {
		// TODO Auto-generated method stub
		Connection co = Connect.getInstance().getConnection();

		// Cr�ation de la requ�te supprimer etablissement
		String requeteSQL = "DELETE FROM `dvd` WHERE id = ?";

		// pr�parer la requ�te
		PreparedStatement pst = co.prepareStatement(requeteSQL);

		// renvoyer et verifier les donn�es de la requ�te
		pst.setInt(1, id);
		
		
		
		int i = pst.executeUpdate();

		return i;
	}
	
	@Override
	public ObservableList<Dvd> findAll() throws ClassNotFoundException, SQLException {
      	 ObservableList<Dvd> DvdList = FXCollections.observableArrayList();

         // constitution d'une commande bas�e sur une requ�te SQL 
         // en vue d'�tre ex�cut�e sur une connexion donn�e     
         String req = "select * from Dvd";

         Connection cnx = Connect.getInstance().getConnection() ;
       int id ;
       String productor;
       int year;
       String libelle;
         PreparedStatement pst = cnx.prepareStatement(req);


         ResultSet jeu = pst.executeQuery();
         while(jeu.next()){
        	 id = jeu.getInt("id");
        	 productor = jeu.getString("productor");
        	 year =jeu.getInt("year");
        	 libelle = jeu.getString("libelle");


        	 
        	 Dvd dvd = new Dvd();
        	 dvd.setId(id);
        	 dvd.setProductor(productor);
        	 dvd.setYear(year);
        	 dvd.setLibelle(libelle);


         }
         jeu.close();
         pst.close();
         cnx.close();
         return DvdList;
	}

	@Override
	public ObservableList<Dvd> findBy(int id) throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		return null;
	}



}
