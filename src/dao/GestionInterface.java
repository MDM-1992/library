package dao;

import java.sql.SQLException;

import javafx.collections.ObservableList;
import metier.User;

public interface GestionInterface<T> {

	int  insert(T t) throws SQLException, ClassNotFoundException;
	int delete(int id) throws SQLException;
	default T verifier(String login, String password)throws SQLException  {return null ;}
	ObservableList<T> findAll() throws ClassNotFoundException, SQLException;
	ObservableList<T> findBy(int id) throws ClassNotFoundException, SQLException;
	
}