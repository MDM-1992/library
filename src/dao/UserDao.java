package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import metier.User;

public class UserDao implements GestionInterface<User>{

	@Override
	public int insert(User user) throws SQLException, ClassNotFoundException {
		// Je me connecte
		Connection co = Connect.getInstance().getConnection();

		// Cr�ation de la requ�te inserer new pers
		String requeteSQL = "INSERT INTO `user`(`login`, `email`, `password`, `role`) "
				+ "VALUES (?,?,?,?)";

		// pr�parer la requ�te
		PreparedStatement pst = co.prepareStatement(requeteSQL);

		// renvoyer et verifier les donn�es de la requ�te
		pst.setString(1, user.getLogin());
		pst.setString(2, user.getEmail());
		pst.setString(3, user.getPassword());
		pst.setString(4, user.getRole());
		;

		int nbligne = pst.executeUpdate();
		return nbligne;	
	}
	
	public  void Update(User user) throws SQLException, ClassNotFoundException {
		// Je me connecte
		Connection co = Connect.getInstance().getConnection();

		// Cr�ation de la requ�te inserer new pers
		String requeteSQL = "UPDATE user SET login= ?,email= ?, password = ?, role = ?WHERE id =?";

		// pr�parer la requ�te
		PreparedStatement pst = co.prepareStatement(requeteSQL);

		// renvoyer et verifier les donn�es de la requ�te
		pst.setString(1, user.getLogin());
		pst.setString(2, user.getEmail());
		pst.setString(3, user.getPassword());
		pst.setString(4, user.getRole());
		
		int nbligne = pst.executeUpdate();

	
		
	}
	
	public static User UserVerif(String login, String password) throws SQLException {

		Connection cnx = Connect.getInstance().getConnection();
		String req = "SELECT id, email, password, role FROM User WHERE login=? AND password=? ";
		PreparedStatement pst = cnx.prepareStatement(req);
		pst.setString(1, login);
	    pst.setString(2, password);
	    User user = null;
		ResultSet userInformation = pst.executeQuery();
		while(userInformation .next()) {

		 String role = userInformation.getString("role") ;
		 int id =   userInformation.getInt("id") ;
		 String mail = userInformation.getString("email");
		 String mdp = userInformation.getString("password");
		 user = new User();
         user.setRole(role);
         user.setId(id);
         user.setEmail(mail);
         user.setPassword(mdp);


		}
		return user;
	}


	@Override
	public int delete(int id) throws SQLException {
		// TODO Auto-generated method stub
		Connection co = Connect.getInstance().getConnection();

		// Cr�ation de la requ�te supprimer etablissement
		String requeteSQL = "DELETE FROM `user` WHERE id = ?";

		// pr�parer la requ�te
		PreparedStatement pst = co.prepareStatement(requeteSQL);

		// renvoyer et verifier les donn�es de la requ�te
		pst.setInt(1, id);
		
		
		
		int i = pst.executeUpdate();

		return i;
	}
	
	@Override
	public ObservableList<User> findAll() throws ClassNotFoundException, SQLException {
      	 ObservableList<User> UserList = FXCollections.observableArrayList();

         // constitution d'une commande bas�e sur une requ�te SQL 
         // en vue d'�tre ex�cut�e sur une connexion donn�e     
         String req = "select * from User";

         Connection cnx = Connect.getInstance().getConnection() ;
       int id ;
       String login;
       String email;
       String password;
       String role;
         PreparedStatement pst = cnx.prepareStatement(req);


         ResultSet jeu = pst.executeQuery();
         while(jeu.next()){
        	 id = jeu.getInt("id");
        	 login = jeu.getString("login");
        	 email =jeu.getString("email");
        	 password = jeu.getString("password");
        	 role = jeu.getString("role");


        	 
        	 User user = new User();
        	 user.setId(id);
        	 user.setLogin(login);
        	 user.setEmail(email);
        	 user.setPassword(password);
        	 user.setRole(role);


         }
         jeu.close();
         pst.close();
         cnx.close();
         return UserList;
	}

	@Override
	public ObservableList<User> findBy(int id) throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		return null;
	}



}


