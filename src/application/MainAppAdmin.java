package application;

import java.io.IOException;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
//import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
//import metier.User;

//-----------------------------------Matiere---------------------------

/**
 * @author marcm
 *
 */
public class MainAppAdmin extends Application {

	@FXML
	private Stage primaryStage;
	@FXML
	private BorderPane adminMenu;
	@FXML
	private FXMLLoader loader;

	@Override
	public void start(Stage primaryStage) {
		this.primaryStage = primaryStage;
		this.primaryStage.setTitle("library");

		initadminMenu();
		// showListeUser();
	}

	/**
	 * Initializes the root layout.
	 */
	public void initadminMenu() {
		try {
			// Load root layout from fxml file.
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainAppAdmin.class.getResource("/vue/adminMenu.fxml"));
			adminMenu = (BorderPane) loader.load();

			// Show the scene containing the root layout.
			Scene scene = new Scene(adminMenu);
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/*
	 * private static void setCenter(Object etablissement) { // TODO Auto-generated
	 * method stub
	 * 
	 * }
	 */

	/**
	 * Returns the main stage.
	 * 
	 * @return
	 */
	public Stage getPrimaryStage() {
		return primaryStage;
	}

	public static void main1(String[] args) {
		launch(args);
	}

	/*  *//**
			 * The data as an observable list of Persons.
			 */
	/*
	 * private ObservableList<Matiere> MatiereData =
	 * FXCollections.observableArrayList();
	 * 
	 * 
	 *//**
		 * Returns the data as an observable list of Persons.
		 * 
		 * @return
		 *//*
			 * public ObservableList<Matiere> getMatiereData() { return MatiereData; }
			 */


	}

